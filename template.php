<?php
/**
 * @file
 * template.php
 */

/**
 * Taxonomy term node count implementation.
 */
function _bootstrap_dashboard_taxonomy_term_node_count($tid, $type = 0) {
  static $count;

  if (isset($count[$type][$tid])) {
    return $count[$type][$tid];
  }

  $query = db_select('taxonomy_index', 't');
  $query->condition('tid', $tid, '=');
  $query->addExpression('COUNT(*)', 'count_nodes');

  // Restrict query by Content Type.
  if (!empty($type)) {
    $query->join('node', 'n', 't.nid = n.nid');
    $query->condition('type', $type, '=');
  }

  $count[$type][$tid] = $query->execute()->fetchField();

  return $count[$type][$tid];
}

/**
 * Implements theme_field().
 */
function bootstrap_dashboard_field__field_tags($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $item) {
    $item['#title'] = $item['#title'] . '&nbsp;<span class="badge">' . _bootstrap_dashboard_taxonomy_term_node_count($item['#options']['entity']->tid) . '</span>';
    $item['#options']['html'] = TRUE;
    $output .= drupal_render($item) . '&nbsp;&nbsp;';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div> <br/>';

  return $output;
}

/**
 * Implements comment_node_view_alter().
 */
function bootstrap_dashboard_node_view_alter(&$build) {
  // Theme node links.
  if (isset($build['links']['node']['#links'])) {
    foreach (array_keys($build['links']['node']['#links']) as $key) {
      $build['links']['node']['#links'][$key]['attributes']['class'][] = 'btn btn-xs btn-primary';
    }
  }

  // Theme comment links.
  if (isset($build['links']['comment']['#links'])) {
    foreach (array_keys($build['links']['comment']['#links']) as $key) {
      switch ($key) {
        case 'comment-comments':
          $build['links']['comment']['#links'][$key]['title'] = t('Comments !count', array('!count' => '<span class="badge">' . $build['#node']->comment_count . '</span>'), array('html' => TRUE));
          $build['links']['comment']['#links'][$key]['attributes']['class'][] = 'btn btn-xs btn-default';
          break;

        case 'comment-new-comments':
          $build['links']['comment']['#links'][$key]['title'] = t('New comments !count', array('!count' => '<span class="badge">' . comment_num_new($build['#node']->nid) . '</span>'), array('html' => TRUE));
          $build['links']['comment']['#links'][$key]['attributes']['class'][] = 'btn btn-xs btn-default';
          break;

        case 'comment-add':
          $build['links']['comment']['#links'][$key]['attributes']['class'][] = 'btn btn-xs btn-success';
          break;

        case 'comment_forbidden':
          // Do not theme like a button.
          break;

        default:
          $build['links']['comment']['#links'][$key]['attributes']['class'][] = 'btn btn-xs btn-primary';
      }
    }
  }
}

/**
 * Implements template_preprocess_comment().
 */
function bootstrap_dashboard_preprocess_comment(&$variables) {
  // Replace title.
  $variables['title'] = $variables['comment']->subject;
}

/**
 * Implements template_preprocess_user_picture().
 */
function bootstrap_dashboard_preprocess_user_picture(&$variables) {
  // Set default user picture.
  if (empty($variables['user_picture']) && theme_get_setting('toggle_comment_user_picture')) {
    $alt = t("@user's picture", array('@user' => format_username($variables['account'])));
    $file = drupal_get_path('theme', 'bootstrap_dashboard') . '/images/default-avatar.png';
    $variables['user_picture'] = theme('image',
      array(
        'path' => $file,
        'alt' => $alt,
        'title' => $alt,
      )
    );
    if (!empty($variables['account']->uid) && user_access('access user profiles')) {
      $attributes = array('attributes' => array('title' => t('View user profile.')), 'html' => TRUE);
      $variables['user_picture'] = l($variables['user_picture'], "user/" . $variables['account']->uid, $attributes);
    }
  }
}

/**
 * Implements hook_comment_view_alter().
 */
function bootstrap_dashboard_comment_view_alter(&$build) {
  // Theme comment links.
  if (isset($build['links']['comment']['#links'])) {
    foreach (array_keys($build['links']['comment']['#links']) as $key) {
      switch ($key) {
        case 'comment-delete':
          $build['links']['comment']['#links'][$key]['attributes']['class'][] = 'btn btn-xs btn-danger';
          break;

        case 'comment-reply':
          $build['links']['comment']['#links'][$key]['attributes']['class'][] = 'btn btn-xs btn-success';
          break;

        case 'comment_forbidden':
          // Do not theme like a button.
          break;

        default:
          $build['links']['comment']['#links'][$key]['attributes']['class'][] = 'btn btn-xs btn-default';
      }
    }
  }
}

/**
 * Returns HTML for a sort icon.
 */
function bootstrap_dashboard_tablesort_indicator($variables) {
  if ($variables['style'] == "asc") {
    return '&nbsp;<i class="glyphicon glyphicon-arrow-down"></i>';
  }
  else {
    return '&nbsp;<i class="glyphicon glyphicon-arrow-up"></i>';
  }
}
